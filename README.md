# Online Gallery

The idea behind the "Online Gallery" is having a gallery of uploaded images on the internet.
So you can use the "components" to actually upload an image or select previously uploaded one.

## Libraries
* Kotlin - for language
* Glide - for loading images
* Android-Image-Cropper - to have the image crop and rotation
* Dagger 2 - for DI
* RxJava/RxAndroid - for reactive programming

For testing:

* Espresso
* Mockito

## For the future (TODO)
* UI/Ux improvements

## To run the app
- You have to setup the server URL. It's located in the `app/build.gradle` file, under the build config constant. So you can easily switch between develop and production environment.