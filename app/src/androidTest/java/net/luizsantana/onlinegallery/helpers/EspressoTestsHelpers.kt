package net.luizsantana.onlinegallery.helpers

/**
 * Created by luiz on 05/09/17.
 */
class Given(execute: () -> Unit) {
    init {
        execute.invoke()
    }
}
class When(execute: () -> Unit) {
    init {
        execute.invoke()
    }
}
class Then(execute: () -> Unit) {
    init {
        execute.invoke()
    }
}