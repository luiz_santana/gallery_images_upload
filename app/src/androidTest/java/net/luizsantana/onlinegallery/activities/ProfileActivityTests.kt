package net.luizsantana.onlinegallery.activities

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import net.luizsantana.onlinegallery.R
import net.luizsantana.onlinegallery.helpers.Given
import net.luizsantana.onlinegallery.helpers.Then
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by luiz on 05/09/17.
 */
@RunWith(AndroidJUnit4::class)
class ProfileActivityTests {

    @get:Rule
    val activityRule = ActivityTestRule(ProfileActivity::class.java)

    @Test
    fun shouldContainGalleryUploaderFragment() {
        Given {
            userIsInProfileActivity()
        }
        Then {
            galleryUploaderFragmentShouldBeVisible()
        }
    }

    private fun galleryUploaderFragmentShouldBeVisible() {
        onView(withId(R.id.fragment_gallery_uploader_container))
                .check(matches(isDisplayed()))
    }

    private fun userIsInProfileActivity() {
        onView(withId(R.id.profile_activity_container))
                .check(matches(isDisplayed()))
    }
}