package net.luizsantana.onlinegallery.activities

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.util.Log
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import net.luizsantana.onlinegallery.R
import net.luizsantana.onlinegallery.common.DaggerInjection
import net.luizsantana.onlinegallery.gallery.adapters.ImagesViewHolder
import net.luizsantana.onlinegallery.gallery.contracts.GalleryListContract
import net.luizsantana.onlinegallery.gallery.models.Image
import net.luizsantana.onlinegallery.gallery.presenters.GalleryListPresenterImpl
import net.luizsantana.onlinegallery.helpers.Given
import net.luizsantana.onlinegallery.helpers.Then
import net.luizsantana.onlinegallery.helpers.When
import net.luizsantana.onlinegallery.modules.TestGalleryListModule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by luiz on 05/09/17.
 */
@RunWith(AndroidJUnit4::class)
class GalleryListActivityTests {
    @get:Rule
    var activityRule = ActivityTestRule(GalleryListActivity::class.java, false, false)

    lateinit var presenter: GalleryListContract.Presenter

    @Before
    fun setup() {
        presenter = mock<GalleryListPresenterImpl>()

        DaggerInjection.galleryListModule = TestGalleryListModule(null, presenter)

        whenever(presenter.loadData())
                .thenAnswer {
                    activityRule.activity?.runOnUiThread {
                        activityRule.activity?.showImageData(listOf(mockImage(), mockImage()))
                    }
                }
        activityRule.launchActivity(Intent())
        presenter.loadData()

    }

    @Test
    fun userShouldSeeAListOfImages() {
        Given {
            userIsInGalleryListActivity()
        }
        Then {
            shouldSeeAListOfImage()
        }
    }

    @Test
    fun userShouldBeAbleToSelectImage() {
        Given {
            userIsInGalleryListActivity()
        }
        When {
            userTapInAPicture()
        }
        Then {
            itShouldSelectTappedPicture()
        }
    }

    private fun shouldSeeAListOfImage() {
        onView(withId(R.id.images_list))
                .check(matches(isDisplayed()))
    }

    private fun itShouldSelectTappedPicture() {
        assert(activityRule.activity.isFinishing)
    }

    private fun userTapInAPicture() {
        onView(withId(R.id.images_list))
                .perform(actionOnItemAtPosition<ImagesViewHolder>(0, ViewActions.click()))
    }

    private fun userIsInGalleryListActivity() {
        onView(withId(R.id.gallery_list_container))
                .check(matches(isDisplayed()))
    }

    private fun mockImage(id: Int = 1) = Image(id = id, picture = "https://media.licdn.com/mpr/mpr/shrink_200_200/AAEAAQAAAAAAAAfGAAAAJGZiYjkwN2M4LWRiNzItNGIxMC04ZWMzLWEwYzM5MmQ2ODg3Mw.png")
}