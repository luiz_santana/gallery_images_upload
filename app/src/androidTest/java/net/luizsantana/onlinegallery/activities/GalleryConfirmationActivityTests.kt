package net.luizsantana.onlinegallery.activities

import android.content.Intent
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.*
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.assertion.ViewAssertions.*
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.widget.ImageView
import net.luizsantana.onlinegallery.R
import net.luizsantana.onlinegallery.gallery.models.Image
import net.luizsantana.onlinegallery.helpers.Then
import net.luizsantana.onlinegallery.helpers.Given
import net.luizsantana.onlinegallery.helpers.When
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by luiz on 11/09/17.
 */
@RunWith(AndroidJUnit4::class)
class GalleryConfirmationActivityTests {
    @get:Rule
    var activityRule = ActivityTestRule(GalleryConfirmationActivity::class.java, false, false)

    @Before
    fun setUp() {
        val intent = Intent()
        intent.putExtra(GalleryConfirmationActivity.IMAGE_KEY, Image(id = 1,
                picture = "http://image/picture",
                favorite = false))
        activityRule.launchActivity(intent)
    }

    @Test
    fun confirmationHasDeleteButton() {
        Given {
            userIsInConfirmationScreen()
        }
        Then {
            shouldSeeADeleteButton()
        }
    }

    @Test
    fun confirmationHasImage() {
        Given {
            userIsInConfirmationScreen()
        }
        Then {
            shouldSeeAImage()
        }
    }

    @Test
    fun confirmationHasFavoriteButton() {
        Given {
            userIsInConfirmationScreen()
        }
        Then {
            shouldSeeAFavoriteButton()
        }
    }

    @Test
    fun confirmationHasSetAsProfileButton() {
        Given {
            userIsInConfirmationScreen()
        }
        Then {
            shouldSeeAProfileButton()
        }
    }

    @Test
    fun confirmationHasCancel() {
        Given {
            userIsInConfirmationScreen()
        }
        Then {
            shouldSeeACancelButton()
        }
    }

    @Test
    fun onToggleFavoriteItShouldUpdateState() {
        Given {
            userIsInConfirmationScreen()
        }
        When {
            userTapsFavoriteIcon()
        }
        Then {
            favoriteIconStateShouldBeUpdated()
        }
    }

    private fun favoriteIconStateShouldBeUpdated() {
    }

    private fun userTapsFavoriteIcon() {
        onView(withId(R.id.button_favorite))
                .perform(click())
    }

    private fun shouldSeeACancelButton() {
        onView(withId(R.id.button_cancel))
                .check(matches(isDisplayed()))
    }

    private fun shouldSeeAFavoriteButton() {
        onView(withId(R.id.button_favorite))
                .check(matches(isDisplayed()))
    }

    private fun shouldSeeAProfileButton() {
        onView(withId(R.id.button_set_as_profile))
                .check(matches(isDisplayed()))
    }

    private fun shouldSeeAImage() {
        onView(withId(R.id.image_confirmation))
                .check(matches(isDisplayed()))
    }


    private fun shouldSeeADeleteButton() {
        onView(withId(R.id.button_confirmation_delete))
                .check(matches(isDisplayed()))
    }

    private fun userIsInConfirmationScreen() {
        onView(withId(R.id.gallery_confirmation_screen_container))
                .check(matches(isDisplayed()))
    }

}
