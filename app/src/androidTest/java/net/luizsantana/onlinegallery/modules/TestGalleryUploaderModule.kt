package net.luizsantana.onlinegallery.modules

import net.luizsantana.onlinegallery.gallery.contracts.GalleryListContract
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryListModule
import net.luizsantana.onlinegallery.gallery.services.GalleryService

/**
 * Created by luiz on 06/09/17.
 */
class TestGalleryListModule(view: GalleryListContract.View?, val presenter: GalleryListContract.Presenter): GalleryListModule(view) {

    override fun provideGalleryListPresenter(service: GalleryService, view: GalleryListContract.View): GalleryListContract.Presenter {
        return presenter
    }

}