package net.luizsantana.onlinegallery.fragments

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import net.luizsantana.onlinegallery.R
import net.luizsantana.onlinegallery.activities.ProfileActivity
import net.luizsantana.onlinegallery.helpers.Given
import net.luizsantana.onlinegallery.helpers.Then
import net.luizsantana.onlinegallery.helpers.When
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Created by luiz on 05/09/17.
 */
@RunWith(AndroidJUnit4::class)
class GalleryUploaderContractTests {

    @get:Rule
    var activityRule = ActivityTestRule(ProfileActivity::class.java)

    @Test
    fun shouldOpenPopupOnClick() {
        Given {
            componentIsVisible()
        }
        When {
            userClicksOnImage()
        }
        Then {
            selectorPopUpShouldAppear()
        }
    }

    @Test
    fun popupShouldShowRightOptions() {
        Given {
            componentIsVisible()
        }
        When {
            userClicksOnImage()
        }
        Then {
            userCanSeeTheGalleryOption()
            userCanSeeTheCameraOption()
            userCanSeeThePreviousUploadedOption()
        }
    }

    @Test
    fun shouldShowImage() {
        Given {
            componentIsVisible()
        }
        Then {
            userCanSeeImage()
        }
    }

    private fun userCanSeeImage() {
        onView(withId(R.id.selected_image))
                .check(matches(isDisplayed()))
    }

    private fun selectorPopUpShouldAppear() {
        onView(withText("Camera"))
                .check(matches(isDisplayed()))
    }

    private fun userClicksOnImage() {
        onView(withId(R.id.selected_image))
                .perform(click())
    }

    private fun componentIsVisible() {
        onView(withId(R.id.fragment_gallery_uploader_container))
                .check(matches(isDisplayed()))
    }

    private fun userCanSeeThePreviousUploadedOption() {
        onView(withText("Prev. Uploaded Image"))
                .check(matches(isDisplayed()))
    }

    private fun userCanSeeTheCameraOption() {
        onView(withText("Camera"))
                .check(matches(isDisplayed()))
    }

    private fun userCanSeeTheGalleryOption() {
        onView(withText("Gallery"))
                .check(matches(isDisplayed()))
    }

}