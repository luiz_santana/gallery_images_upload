package net.luizsantana.onlinegallery.gallery.presenters

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import net.luizsantana.onlinegallery.gallery.contracts.GalleryConfirmationContract
import net.luizsantana.onlinegallery.gallery.models.Image
import net.luizsantana.onlinegallery.gallery.services.GalleryService
import javax.inject.Inject

/**
 * Created by luiz on 11/09/17.
 */
class GalleryConfirmationPresenterImpl
        @Inject constructor(var view: GalleryConfirmationContract.View, var service: GalleryService): GalleryConfirmationContract.Presenter {

    lateinit var disposable: CompositeDisposable

    override fun onAttach() {
        disposable = CompositeDisposable()
    }

    override fun onDettach() {
        disposable.clear()
    }

    override fun updateFavoriteState(image: Image) {
        image.favorite = !image.favorite
        view.showLoadingState()
        disposable.add(
                service.updateImage(image.id, image)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        view.favoriteStateUpdate(it)
                        view.hideLoadingState()
                    },
                    { view.showErrorMessage() }))
    }

    override fun deleteImage(image: Image) {
        view.showLoadingState()
        disposable.add(
                service.deleteImage(image.id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ view.closeAsCancel() },
                                { view.showErrorMessage() }))
    }

    override fun selectAsProfilePicture(image: Image) {
        view.showLoadingState()
        disposable.add(
                service.selectImage(image.id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ view.closeAsSuccess() },
                            { view.showErrorMessage() }))
    }
}