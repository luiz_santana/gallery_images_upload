package net.luizsantana.onlinegallery.gallery.di.modules

import dagger.Module
import dagger.Provides
import net.luizsantana.onlinegallery.gallery.contracts.GalleryListContract
import net.luizsantana.onlinegallery.gallery.presenters.GalleryListPresenterImpl
import net.luizsantana.onlinegallery.gallery.services.GalleryService

/**
 * Created by luiz on 05/09/17.
 */
@Module
open class GalleryListModule(var view: GalleryListContract.View?) {

    @Provides
    fun provideGalleryListView() = view!!

    @Provides
    open fun provideGalleryListPresenter(service: GalleryService, view: GalleryListContract.View): GalleryListContract.Presenter =
            GalleryListPresenterImpl(view, service)
}