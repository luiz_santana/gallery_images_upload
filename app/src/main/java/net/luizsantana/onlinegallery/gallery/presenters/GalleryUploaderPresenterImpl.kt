package net.luizsantana.onlinegallery.gallery.presenters

import android.net.Uri
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import net.luizsantana.onlinegallery.gallery.contracts.GalleryUploaderContract
import net.luizsantana.onlinegallery.gallery.models.Image
import net.luizsantana.onlinegallery.gallery.services.GalleryService
import okhttp3.MediaType
import java.io.File
import javax.inject.Inject
import okhttp3.RequestBody
import okhttp3.MultipartBody





/**
 * Created by luiz on 04/09/17.
 */
class GalleryUploaderPresenterImpl @Inject constructor(val view: GalleryUploaderContract.View,
                                                       val galleryService: GalleryService): GalleryUploaderContract.Presenter {
    override fun imagePickCanceled() {
        view.showPlaceholder()
    }

    lateinit var disposable: CompositeDisposable

    override fun onAttach() {
        disposable = CompositeDisposable()
    }

    override fun onDettach() {
        disposable.clear()
    }

    override fun loadPreviouslySelectedImage() {
        view.showLoading()

        disposable.add(
            galleryService.loadSelectedImage()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        if (it != null) {
                            view.showSelectedImage(it)
                        } else {
                            view.showPlaceholder()
                        }
                    },
                    {
                        view.showPlaceholder()
                    }))
    }

    override fun uploadImage(uri: Uri) {
        view.showLoading()
        val file = File(uri.path)
        val requestFile = RequestBody.create(MediaType.parse("image/jpeg"), file)
        val body = MultipartBody.Part.createFormData("image[picture]", file.name, requestFile)
        disposable.add(
            galleryService.uploadImage(body)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ setImageAsSelected(it) },
                            {
                                view.showErrorMessage()
                            }))
    }

    override fun setImageAsSelected(image: Image) {
        view.showLoading()
        disposable.add(
            galleryService.selectImage(image.id)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ view.showSelectedImage(it) },
                            {
                                view.showErrorMessage()
                            }))
    }
}