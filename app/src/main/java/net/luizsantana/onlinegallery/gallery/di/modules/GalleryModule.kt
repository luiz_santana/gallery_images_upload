package net.luizsantana.onlinegallery.gallery.di.modules

import dagger.Module
import dagger.Provides
import net.luizsantana.onlinegallery.gallery.services.GalleryService
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by luiz on 05/09/17.
 */
@Module
class GalleryModule {

    @Provides @Singleton
    fun providesGalleryService(retrofit: Retrofit): GalleryService = retrofit.create(GalleryService::class.java)

}