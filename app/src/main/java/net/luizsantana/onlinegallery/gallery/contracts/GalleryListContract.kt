package net.luizsantana.onlinegallery.gallery.contracts

import net.luizsantana.onlinegallery.common.BasePresenter
import net.luizsantana.onlinegallery.gallery.models.Image

/**
 * Created by luiz on 04/09/17.
 */
interface GalleryListContract {

    interface View {
        fun showEmptyListMessage()
        fun showLoading()
        fun showErrorMessage()
        fun showImageData(images: List<Image>)
    }

    interface Presenter: BasePresenter {
        fun loadData()
    }
}