package net.luizsantana.onlinegallery.gallery.services

import io.reactivex.Completable
import io.reactivex.Observable
import net.luizsantana.onlinegallery.gallery.models.Image
import okhttp3.MultipartBody
import java.io.File

/**
 * Created by luiz on 04/09/17.
 */
class MockGalleryService: GalleryService {
    override fun updateImage(imageId: Int, image: Image): Observable<Image> {
        TODO("not implemented")
    }

    override fun deleteImage(imageId: Int): Completable {
        TODO("not implemented")
    }

    override fun loadSelectedImage(): Observable<Image?> {
        return Observable.just(mockImage())
    }

    override fun uploadImage(file: MultipartBody.Part): Observable<Image> {
        return Observable.just(mockImage())
    }

    override fun selectImage(imageId: Int): Observable<Image> {
        return Observable.just(mockImage())
    }

    override fun listImages(): Observable<List<Image>> {
        return Observable.just(listOf(
                mockImage(id = 1),
                mockImage(id = 2),
                mockImage(id = 3),
                mockImage(id = 4),
                mockImage(id = 5),
                mockImage(id = 6),
                mockImage(id = 7),
                mockImage(id = 8),
                mockImage(id = 9),
                mockImage(id = 10),
                mockImage(id = 11)))
    }

    private fun mockImage(id: Int = 1) = Image(id = id, picture = "https://media.licdn.com/mpr/mpr/shrink_200_200/AAEAAQAAAAAAAAfGAAAAJGZiYjkwN2M4LWRiNzItNGIxMC04ZWMzLWEwYzM5MmQ2ODg3Mw.png")
}