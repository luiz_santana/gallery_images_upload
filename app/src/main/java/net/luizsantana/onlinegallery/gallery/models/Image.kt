package net.luizsantana.onlinegallery.gallery.models

import android.net.Uri
import net.luizsantana.onlinegallery.BuildConfig
import java.io.Serializable

/**
 * Created by luiz on 04/09/17.
 */
data class Image(val id: Int,
                 var picture: String,
                 var favorite: Boolean = false): Serializable {
    fun asUri(): Uri {
        return Uri.parse("${BuildConfig.SERVER_URL}$picture")
    }
}