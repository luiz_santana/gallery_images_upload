package net.luizsantana.onlinegallery.gallery.contracts

import android.net.Uri
import android.support.v4.app.FragmentActivity
import net.luizsantana.onlinegallery.common.BasePresenter
import net.luizsantana.onlinegallery.gallery.models.Image

/**
 * Created by luiz on 04/09/17.
 */
interface GalleryUploaderContract {

    interface View {
        fun showOptions()
        fun showSelectedImage(image: Image)
        fun showPlaceholder()
        fun showLoading()
        fun showErrorMessage()
    }

    interface Presenter : BasePresenter {
        fun loadPreviouslySelectedImage()
        fun uploadImage(uri: Uri)
        fun imagePickCanceled()
        fun setImageAsSelected(image: Image)
    }

}