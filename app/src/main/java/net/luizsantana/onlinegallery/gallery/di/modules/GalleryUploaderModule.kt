package net.luizsantana.onlinegallery.gallery.di.modules

import dagger.Module
import dagger.Provides
import net.luizsantana.onlinegallery.gallery.contracts.GalleryListContract
import net.luizsantana.onlinegallery.gallery.contracts.GalleryUploaderContract
import net.luizsantana.onlinegallery.gallery.presenters.GalleryUploaderPresenterImpl
import net.luizsantana.onlinegallery.gallery.services.GalleryService

/**
 * Created by luiz on 05/09/17.
 */
@Module
class GalleryUploaderModule(var view: GalleryUploaderContract.View?) {

    @Provides
    fun provideGalleryUploaderView() = view!!

    @Provides
    fun provideGalleryUploaderPresenter(view: GalleryUploaderContract.View, service: GalleryService): GalleryUploaderContract.Presenter
            = GalleryUploaderPresenterImpl(view, service)

}