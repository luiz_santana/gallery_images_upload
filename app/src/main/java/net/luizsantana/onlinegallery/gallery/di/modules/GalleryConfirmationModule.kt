package net.luizsantana.onlinegallery.gallery.di.modules

import dagger.Module
import dagger.Provides
import net.luizsantana.onlinegallery.gallery.contracts.GalleryConfirmationContract
import net.luizsantana.onlinegallery.gallery.presenters.GalleryConfirmationPresenterImpl
import net.luizsantana.onlinegallery.gallery.services.GalleryService

/**
 * Created by luiz on 11/09/17.
 */
@Module
class GalleryConfirmationModule(var view: GalleryConfirmationContract.View) {

    @Provides
    fun providesGalleryConfirmationPresenter(service: GalleryService): GalleryConfirmationContract.Presenter {
        return GalleryConfirmationPresenterImpl(view, service)
    }

}