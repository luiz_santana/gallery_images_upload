package net.luizsantana.onlinegallery.gallery.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.subjects.PublishSubject
import net.luizsantana.onlinegallery.R
import net.luizsantana.onlinegallery.common.inflate
import net.luizsantana.onlinegallery.common.loadImage
import net.luizsantana.onlinegallery.gallery.models.Image

/**
 * Created by luiz on 06/09/17.
 */
class ImagesAdapter(val data: List<Image>, val subject: PublishSubject<Image>): RecyclerView.Adapter<ImagesViewHolder>() {
    override fun onBindViewHolder(holder: ImagesViewHolder?, position: Int) {
        holder?.bind(data[position])
    }

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ImagesViewHolder {
        return ImagesViewHolder(parent?.inflate(R.layout.item_image), subject)
    }
}

class ImagesViewHolder(itemView: View?, val subject: PublishSubject<Image>): RecyclerView.ViewHolder(itemView) {
    private val imageView: ImageView by lazy { itemView?.findViewById(R.id.item_image) as ImageView }

    fun bind(image: Image) {
        imageView.loadImage(image.asUri())

        RxView.clicks(imageView)
                .subscribe { this.subject.onNext(image) }
    }

}