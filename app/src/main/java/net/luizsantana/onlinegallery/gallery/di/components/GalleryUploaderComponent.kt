package net.luizsantana.onlinegallery.gallery.di.components

import dagger.Component
import net.luizsantana.onlinegallery.common.modules.NetworkModule
import net.luizsantana.onlinegallery.fragments.GalleryUploaderFragment
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryModule
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryUploaderModule
import javax.inject.Singleton

/**
 * Created by luiz on 05/09/17.
 */
@Singleton
@Component(modules = arrayOf(GalleryUploaderModule::class, GalleryModule::class, NetworkModule::class))
interface GalleryUploaderComponent {
    fun inject(galleryUploaderFragment: GalleryUploaderFragment)
}