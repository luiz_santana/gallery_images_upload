package net.luizsantana.onlinegallery.gallery.contracts

import net.luizsantana.onlinegallery.common.BasePresenter
import net.luizsantana.onlinegallery.gallery.models.Image

/**
 * Created by luiz on 11/09/17.
 */
interface GalleryConfirmationContract {

    interface View  {
        fun showLoadingState()
        fun hideLoadingState()
        fun showErrorMessage()
        fun closeAsSuccess()
        fun closeAsCancel()
        fun favoriteStateUpdate(image: Image)
    }

    interface Presenter: BasePresenter {
        fun updateFavoriteState(image: Image)
        fun deleteImage(image: Image)
        fun selectAsProfilePicture(image: Image)
    }

}