package net.luizsantana.onlinegallery.gallery.services

import io.reactivex.Completable
import io.reactivex.Observable
import net.luizsantana.onlinegallery.gallery.models.Image
import okhttp3.MultipartBody
import retrofit2.http.*
import java.io.File

/**
 * Created by luiz on 04/09/17.
 */
interface GalleryService {
    @Multipart
    @POST("/images.json")
    fun uploadImage(@Part file: MultipartBody.Part): Observable<Image>

    @POST("/images/{imageId}/select.json")
    fun selectImage(@Path("imageId") imageId: Int): Observable<Image>

    @GET("/images.json")
    fun listImages(): Observable<List<Image>>

    @GET("/images/selected.json")
    fun loadSelectedImage(): Observable<Image?>

    @PUT("/images/{imageId}.json")
    fun updateImage(@Path("imageId") imageId: Int, @Body image: Image): Observable<Image>

    @DELETE("/images/{imageId}.json")
    fun deleteImage(@Path("imageId") imageId: Int): Completable
}