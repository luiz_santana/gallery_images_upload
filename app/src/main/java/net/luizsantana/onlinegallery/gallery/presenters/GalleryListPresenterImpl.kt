package net.luizsantana.onlinegallery.gallery.presenters

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import net.luizsantana.onlinegallery.gallery.contracts.GalleryListContract
import net.luizsantana.onlinegallery.gallery.services.GalleryService
import javax.inject.Inject

/**
 * Created by luiz on 04/09/17.
 */
open class GalleryListPresenterImpl @Inject constructor(val galleryListView: GalleryListContract.View,
                                                   val galleryService: GalleryService): GalleryListContract.Presenter {

    lateinit var disposable: CompositeDisposable

    override fun onAttach() {
        disposable = CompositeDisposable()
    }

    override fun onDettach() {
        disposable.clear()
    }

    override fun loadData() {
        galleryListView.showLoading()

        disposable.add(
                galleryService.listImages()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        if (it.isEmpty()) {
                            galleryListView.showEmptyListMessage()
                        } else {
                            galleryListView.showImageData(it)
                        }
                    },
                    { galleryListView.showErrorMessage() }))
    }
}