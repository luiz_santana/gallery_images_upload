package net.luizsantana.onlinegallery.gallery.di.components

import dagger.Component
import net.luizsantana.onlinegallery.activities.GalleryListActivity
import net.luizsantana.onlinegallery.common.modules.NetworkModule
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryListModule
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryModule
import javax.inject.Singleton

/**
 * Created by luiz on 06/09/17.
 */
@Singleton
@Component(modules = arrayOf(GalleryListModule::class, GalleryModule::class, NetworkModule::class))
interface GalleryListComponent {
    fun inject(galleryListActivity: GalleryListActivity)
}