package net.luizsantana.onlinegallery.activities

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable

import net.luizsantana.onlinegallery.R
import net.luizsantana.onlinegallery.common.DaggerInjection
import net.luizsantana.onlinegallery.common.hide
import net.luizsantana.onlinegallery.common.loadImage
import net.luizsantana.onlinegallery.common.show
import net.luizsantana.onlinegallery.gallery.contracts.GalleryConfirmationContract
import net.luizsantana.onlinegallery.gallery.models.Image
import javax.inject.Inject

class GalleryConfirmationActivity : AppCompatActivity(), GalleryConfirmationContract.View {

    companion object {
        val IMAGE_KEY = ""
    }

    private val cancelButton: Button by lazy { findViewById(R.id.button_cancel) as Button }
    private val favoriteButton: ImageButton by lazy { findViewById(R.id.button_favorite) as ImageButton }
    private val setAsProfileButton: Button by lazy { findViewById(R.id.button_set_as_profile) as Button }
    private val deleteButton: Button by lazy { findViewById(R.id.button_confirmation_delete) as Button }
    private val imageView: ImageView by lazy { findViewById(R.id.image_confirmation) as ImageView }
    private val loading: View by lazy { findViewById(R.id.loading_progress) }

    var disposable: CompositeDisposable = CompositeDisposable()
    private var dialog: AlertDialog? = null

    @Inject
    lateinit var presenter: GalleryConfirmationContract.Presenter
    private lateinit var image: Image

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery_confirmation)
        initializeData(savedInstanceState)

        DaggerInjection.inject(this)
    }

    private fun initializeData(savedInstanceState: Bundle?) {
        if (savedInstanceState?.containsKey(IMAGE_KEY) ?: false) {
            image = savedInstanceState?.getSerializable(IMAGE_KEY) as Image
        } else if (intent.hasExtra(IMAGE_KEY)) {
            image = intent.getSerializableExtra(IMAGE_KEY) as Image
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        initializeData(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putSerializable(IMAGE_KEY, image)
    }

    override fun onResume() {
        super.onResume()
        presenter.onAttach()
        bind()
    }

    private fun bind() {
        disposable.add(RxView.clicks(cancelButton)
                .subscribe { finish() })
        disposable.add(RxView.clicks(favoriteButton)
                .subscribe { presenter.updateFavoriteState(image) })
        disposable.add(RxView.clicks(setAsProfileButton)
                .subscribe { presenter.selectAsProfilePicture(image) })
        disposable.add(RxView.clicks(deleteButton)
                .subscribe { presenter.deleteImage(image) })
        favoriteStateUpdate(image)
        imageView.loadImage(image.asUri())
    }

    override fun onPause() {
        super.onPause()
        presenter.onDettach()
        dialog?.dismiss()
    }

    override fun showLoadingState() {
        loading.show()
    }

    override fun hideLoadingState() {
        loading.hide()
    }

    override fun showErrorMessage() {
        dialog = AlertDialog.Builder(this)
                .setMessage(R.string.general_error_message)
                .show()
    }

    override fun closeAsSuccess() {
        val intent = Intent()
        intent.putExtra("data", image)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun closeAsCancel() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun favoriteStateUpdate(image: Image) {
        when(image.favorite) {
            true -> favoriteButton.setImageResource(R.drawable.ic_favorite_selected)
            else -> favoriteButton.setImageResource(R.drawable.ic_favorite_not_selected)
        }
    }
}
