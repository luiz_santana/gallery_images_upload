package net.luizsantana.onlinegallery.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup

import net.luizsantana.onlinegallery.R
import net.luizsantana.onlinegallery.fragments.GalleryUploaderFragment

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        loadFragment()
    }

    private fun loadFragment() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.uploader_container, GalleryUploaderFragment())
                .commit()
    }
}
