package net.luizsantana.onlinegallery.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Toast
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import net.luizsantana.onlinegallery.R
import net.luizsantana.onlinegallery.common.DaggerInjection
import net.luizsantana.onlinegallery.common.hide
import net.luizsantana.onlinegallery.common.show
import net.luizsantana.onlinegallery.gallery.adapters.ImagesAdapter
import net.luizsantana.onlinegallery.gallery.contracts.GalleryListContract
import net.luizsantana.onlinegallery.gallery.models.Image
import javax.inject.Inject

class GalleryListActivity : AppCompatActivity(), GalleryListContract.View {

    companion object {
        val CONFIMARTION = 11
    }

    @Inject
    lateinit var presenter: GalleryListContract.Presenter
    private val loadingView: View by lazy { findViewById(R.id.loading_progress) }
    private val emptyListMessage: View by lazy { findViewById(R.id.empty_list_message) }
    private val imagesList: RecyclerView by lazy { findViewById(R.id.images_list) as RecyclerView }
    private val clickSubject: PublishSubject<Image> = PublishSubject.create()
    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeViews()

        DaggerInjection.inject(this)
    }

    private fun initializeViews() {
        setContentView(R.layout.activity_gallery_list)
        imagesList.layoutManager = GridLayoutManager(this, 3)
    }

    override fun onResume() {
        super.onResume()
        presenter.onAttach()
        disposable.add(
                clickSubject.subscribe { openConfirmationScreen(it) })
        presenter.loadData()
    }

    private fun openConfirmationScreen(it: Image) {
        val intent = Intent(this, GalleryConfirmationActivity::class.java)
        intent.putExtra(GalleryConfirmationActivity.IMAGE_KEY, it)
        startActivityForResult(intent, CONFIMARTION)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == CONFIMARTION) {
            setAsResult(data?.getSerializableExtra("data") as Image)
            finish()
        }
    }

    private fun setAsResult(it: Image) {
        val intent = Intent()
        intent.putExtra("data", it)
        setResult(Activity.RESULT_OK, intent)
    }

    override fun onPause() {
        super.onPause()
        presenter.onDettach()
        disposable.clear()
    }

    override fun showEmptyListMessage() {
        emptyListMessage.show()
        loadingView.hide()
    }

    override fun showLoading() {
        loadingView.show()
    }

    override fun showErrorMessage() {
        loadingView.hide()
        Toast.makeText(this, R.string.general_error_message, Toast.LENGTH_LONG)
    }

    override fun showImageData(images: List<Image>) {
        loadingView.hide()
        imagesList.adapter = ImagesAdapter(images, clickSubject)
    }
}
