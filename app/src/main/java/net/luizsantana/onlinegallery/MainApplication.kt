package net.luizsantana.onlinegallery

import android.app.Application
import net.luizsantana.onlinegallery.common.DaggerInjection
import android.os.StrictMode



/**
 * Created by lsantana on 06/09/2017.
 */
class MainApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        DaggerInjection.init(this, BuildConfig.SERVER_URL)
    }

}