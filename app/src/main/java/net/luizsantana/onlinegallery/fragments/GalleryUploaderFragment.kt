package net.luizsantana.onlinegallery.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.jakewharton.rxbinding2.view.RxView
import com.theartofdev.edmodo.cropper.CropImage
import io.reactivex.disposables.CompositeDisposable
import net.luizsantana.onlinegallery.R
import net.luizsantana.onlinegallery.activities.GalleryListActivity
import net.luizsantana.onlinegallery.common.*
import net.luizsantana.onlinegallery.gallery.contracts.GalleryUploaderContract
import net.luizsantana.onlinegallery.gallery.di.components.DaggerGalleryUploaderComponent
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryModule
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryUploaderModule
import net.luizsantana.onlinegallery.gallery.models.Image
import java.io.File
import java.util.*
import javax.inject.Inject


/**
 * Created by luiz on 05/09/17.
 */
class GalleryUploaderFragment : Fragment(), GalleryUploaderContract.View {

    val PICK_IMAGE = 1024
    val PICK_UPLADED_IMAGE = 1025
    val TAKE_PHOTO = 1026

    @Inject
    lateinit var presenter: GalleryUploaderContract.Presenter
    private val selectedImage: ImageView by lazy { activity.findViewById(R.id.selected_image) as ImageView }
    private val loadingView: View by lazy { activity.findViewById(R.id.loading_progress) }
    private val disposable = CompositeDisposable()
    private var dialog: AlertDialog? = null
    private var tempUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_gallery_uploader)
    }

    override fun onResume() {
        super.onResume()
        presenter.onAttach()
        disposable.add(RxView.clicks(selectedImage)
                .subscribe { showOptions() })
        presenter.loadPreviouslySelectedImage()
    }

    override fun onPause() {
        super.onPause()
        presenter.onDettach()
        disposable.clear()
        dialog?.dismiss()
        dialog = null
    }

    override fun showOptions() {
        dialog = AlertDialog.Builder(context)
                .setTitle(R.string.options_title)
                .setItems(optionsItems(), { dialog, which ->
                    when(optionsItems()[which]) {
                        "Camera" -> openCameraActivity()
                        "Gallery" -> openGalleryActivity()
                        "Prev. Uploaded Image" -> openGalleryListActivity()
                        else -> dialog.dismiss()
                    }
                })
                .show()
    }

    private fun openGalleryListActivity() {
        val intent = Intent(context, GalleryListActivity::class.java)
        startActivityForResult(intent, PICK_UPLADED_IMAGE)
    }

    private fun openGalleryActivity() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
    }

    private fun openCameraActivity() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        tempUri = Uri.fromFile(File(Environment.getExternalStorageDirectory(), "/temp_pic${Date().time}.jpg"))
        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivityForResult(intent, TAKE_PHOTO)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            presenter.imagePickCanceled()
            return
        }

        when(requestCode) {
            PICK_IMAGE -> retrieveData(data)
            PICK_UPLADED_IMAGE -> retrieveUploadedData(data)
            TAKE_PHOTO -> retrievePhotoData()
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> uploadData(data)
        }
    }

    private fun retrievePhotoData() {
        CropImage.activity(tempUri)
                .setAllowRotation(true)
                .setAspectRatio(1, 1)
                .start(context, this)
    }

    private fun uploadData(data: Intent?) {
        presenter.uploadImage(CropImage.getActivityResult(data).uri)
    }

    private fun retrieveUploadedData(data: Intent?) {
        data?.extras?.let {
            val image = it.get("data") as Image
            presenter.setImageAsSelected(image)
        }
    }

    private fun retrieveData(data: Intent?) {
        data?.data?.let {
            CropImage.activity(it)
                    .setAllowRotation(true)
                    .setAspectRatio(1, 1)
                    .start(context, this)
        }
    }

    fun optionsItems() = arrayOf("Camera", "Gallery", "Prev. Uploaded Image")

    override fun showSelectedImage(image: Image) {
        if (!isAdded) {
            return
        }
        loadingView.hide()
        selectedImage.loadImage(image.asUri())
    }

    override fun showPlaceholder() {
        selectedImage.setImageResource(R.drawable.ic_placeholder)
        loadingView.hide()
    }

    override fun showLoading() {
        selectedImage.setImageBitmap(null)
        loadingView.show()
    }

    override fun showErrorMessage() {
        loadingView.hide()
        Toast.makeText(context, R.string.general_error_message, Toast.LENGTH_LONG)
        selectedImage.setImageResource(R.drawable.ic_error)
    }
}