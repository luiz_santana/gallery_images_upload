@file:JvmName("AndroidUtils")
package net.luizsantana.onlinegallery.common

import android.app.Activity
import android.net.Uri
import android.support.annotation.LayoutRes
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import net.luizsantana.onlinegallery.R
import java.io.File
import java.lang.Exception

/**
 * Created by luiz on 05/09/17.
 */
fun View.inflate(@LayoutRes id: Int): View {
    return LayoutInflater.from(context).inflate(id, null, false)
}
fun View.hide() {
    this.visibility = View.GONE
}
fun View.show() {
    this.visibility = View.VISIBLE
}

fun ImageView.loadImage(uri: Uri) {
    Glide.with(context)
            .load(uri)
            .fitCenter()
            .fallback(R.drawable.ic_placeholder)
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_error)
            .listener(object: RequestListener<Uri, GlideDrawable> {
                override fun onException(e: Exception?, model: Uri?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                    e?.printStackTrace()
                    return false
                }

                override fun onResourceReady(resource: GlideDrawable?, model: Uri?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                    return false
                }

            })
            .into(this)
}