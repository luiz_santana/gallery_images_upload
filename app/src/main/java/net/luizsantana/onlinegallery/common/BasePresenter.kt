package net.luizsantana.onlinegallery.common

/**
 * Created by luiz on 04/09/17.
 */
interface BasePresenter {
    fun onAttach()
    fun onDettach()
}