package net.luizsantana.onlinegallery.common

import android.app.Application
import net.luizsantana.onlinegallery.activities.GalleryConfirmationActivity
import net.luizsantana.onlinegallery.activities.GalleryListActivity
import net.luizsantana.onlinegallery.common.modules.NetworkModule
import net.luizsantana.onlinegallery.fragments.GalleryUploaderFragment
import net.luizsantana.onlinegallery.gallery.di.components.DaggerGalleryConfirmationComponent
import net.luizsantana.onlinegallery.gallery.di.components.DaggerGalleryListComponent
import net.luizsantana.onlinegallery.gallery.di.components.DaggerGalleryUploaderComponent
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryConfirmationModule
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryListModule
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryModule
import net.luizsantana.onlinegallery.gallery.di.modules.GalleryUploaderModule

/**
 * Created by lsantana on 06/09/2017.
 */
class DaggerInjection {
    companion object {
        lateinit var networkModule: NetworkModule
        var galleryModule: GalleryModule? = null
        var galleryUploaderModule: GalleryUploaderModule? = null
        var galleryListModule: GalleryListModule? = null
        var galleryConfirmationModule: GalleryConfirmationModule? = null

        fun init(application: Application, url: String) {
            networkModule = NetworkModule(application, url)
        }

        fun inject(view: GalleryListActivity) {
            initGalleryModule()
            initGalleryListModule(view)
            DaggerGalleryListComponent.builder()
                    .galleryListModule(galleryListModule)
                    .galleryModule(galleryModule)
                    .networkModule(networkModule)
                    .build()
                    .inject(view)
        }

        private fun initGalleryListModule(view: GalleryListActivity) {
            if (galleryListModule == null) {
                galleryListModule = GalleryListModule(view)
            } else {
                galleryListModule?.view = view
            }
        }

        private fun initGalleryModule() {
            if (galleryModule == null) {
                galleryModule = GalleryModule()
            }
        }

        fun inject(view: GalleryUploaderFragment) {
            initGalleryModule()
            initGalleryUploaderModule(view)
            DaggerGalleryUploaderComponent.builder()
                    .galleryModule(galleryModule)
                    .galleryUploaderModule(galleryUploaderModule)
                    .networkModule(networkModule)
                    .build()
                    .inject(view)
        }

        private fun initGalleryUploaderModule(view: GalleryUploaderFragment) {
            if (galleryUploaderModule == null) {
                galleryUploaderModule = GalleryUploaderModule(view)
            } else {
                galleryUploaderModule?.view = view
            }
        }

        fun inject(activity: GalleryConfirmationActivity) {
            initGalleryModule()
            if (galleryConfirmationModule == null) {
                galleryConfirmationModule = GalleryConfirmationModule(activity)
            } else {
                galleryConfirmationModule?.view = activity
            }

            DaggerGalleryConfirmationComponent.builder()
                    .galleryConfirmationModule(galleryConfirmationModule)
                    .networkModule(networkModule)
                    .galleryModule(galleryModule)
                    .build()
                    .inject(activity)
        }
    }
}