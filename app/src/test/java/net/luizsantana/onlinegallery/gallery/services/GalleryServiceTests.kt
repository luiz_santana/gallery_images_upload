package net.luizsantana.onlinegallery.gallery.services

import com.nhaarman.mockito_kotlin.any
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import net.luizsantana.onlinegallery.gallery.models.Image
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import java.io.File

/**
 * Created by luiz on 04/09/17.
 */
class GalleryServiceTests {

    lateinit var service: GalleryService

    @Test
    fun serviceShouldHaveAbilityToUploadImage() {
        service = Mockito.mock(GalleryService::class.java)
        `when`(service.uploadImage(any()))
                .thenReturn(Observable.just(mockImage()))

        val testObserver = TestObserver<Image>()

        val file = File("file")
        val requestFile = RequestBody.create(MediaType.parse("images/*"), file)
        val body = MultipartBody.Part.createFormData("image[picture]", file.name, requestFile)
        service.uploadImage(body)
                .subscribe(testObserver)

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
    }

    @Test
    fun serviceShouldHaveAbilityToSelectImage() {
        service = Mockito.mock(GalleryService::class.java)
        `when`(service.selectImage(any()))
                .thenReturn(Observable.just(mockImage()))

        val testObserver = TestObserver<Image>()

        service.selectImage(mockImage().id)
                .subscribe(testObserver)

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
    }

    @Test
    fun serviceShouldHaveAbilityToLoadSelectedImage() {
        service = Mockito.mock(GalleryService::class.java)
        `when`(service.loadSelectedImage())
                .thenReturn(Observable.just(mockImage()))

        val testObserver = TestObserver<Image>()

        service.loadSelectedImage()
                .subscribe(testObserver)

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
    }

    @Test
    fun serviceShouldHaveAbilityToListImages() {
        service = Mockito.mock(GalleryService::class.java)
        val listOfImages = listOf(mockImage(1),
                mockImage(2),
                mockImage(3),
                mockImage(4))
        `when`(service.listImages())
                .thenReturn(Observable.just(
                        listOfImages))

        val testObserver = TestObserver<List<Image>>()

        service.listImages()
                .subscribe(testObserver)

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
    }

    private fun mockImage(id: Int = 1) = Image(id = id,
            picture = "http://image/picture")
}