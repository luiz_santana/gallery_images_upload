package net.luizsantana.onlinegallery.gallery.presenters

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import net.luizsantana.onlinegallery.gallery.contracts.GalleryConfirmationContract
import net.luizsantana.onlinegallery.gallery.helpers.mockImage
import net.luizsantana.onlinegallery.gallery.services.GalleryService
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

/**
 * Created by luiz on 11/09/17.
 */
class GalleryConfirmationPresenterTests {

    lateinit var service: GalleryService
    lateinit var view: GalleryConfirmationContract.View
    lateinit var presenter: GalleryConfirmationContract.Presenter

    @Before
    fun setUp() {
        service = mock(GalleryService::class.java)
        view = mock(GalleryConfirmationContract.View::class.java)
        presenter = GalleryConfirmationPresenterImpl(view, service)
        presenter.onAttach()

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    }

    @After
    fun tearDown() {
        presenter.onDettach()
    }

    @Test
    fun onUpdateStatusShouldCallStatusUpdate() {
        `when`(service.updateImage(any(), any()))
                .thenReturn(Observable.just(mockImage()))

        presenter.updateFavoriteState(mockImage())

        verify(view, times(1)).favoriteStateUpdate(mockImage())
    }

    @Test
    fun onDeleteImageShouldCloseAsCanceled() {
        `when`(service.deleteImage(any()))
                .thenReturn(Completable.complete())

        presenter.deleteImage(mockImage())

        verify(view, times(1)).closeAsCancel()
    }

    @Test
    fun onSelectPictureAsProfileShouldCloseAsSuccess() {
        `when`(service.selectImage(any()))
                .thenReturn(Observable.just(mockImage()))

        presenter.selectAsProfilePicture(mockImage())

        verify(view, times(1)).closeAsSuccess()
    }

    @Test
    fun onErrorShouldShowErrorMessage() {
        `when`(service.selectImage(any()))
                .thenReturn(Observable.error(RuntimeException("Error")))

        presenter.selectAsProfilePicture(mockImage())

        verify(view, times(1)).showErrorMessage()
    }
}