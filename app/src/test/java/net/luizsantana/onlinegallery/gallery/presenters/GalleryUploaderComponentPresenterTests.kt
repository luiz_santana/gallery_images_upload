package net.luizsantana.onlinegallery.gallery.presenters

import android.net.Uri
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.atLeast
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import net.luizsantana.onlinegallery.gallery.contracts.GalleryUploaderContract
import net.luizsantana.onlinegallery.gallery.models.Image
import net.luizsantana.onlinegallery.gallery.services.GalleryService
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.lang.RuntimeException

/**
 * Created by luiz on 04/09/17.
 */
class GalleryUploaderComponentPresenterTests {

    lateinit var service: GalleryService
    lateinit var view: GalleryUploaderContract.View
    lateinit var presenter: GalleryUploaderPresenterImpl

    @Before
    fun setUp() {
        service = mock(GalleryService::class.java)
        view = mock(GalleryUploaderContract.View::class.java)
        presenter = GalleryUploaderPresenterImpl(view, service)
        presenter.onAttach()

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    }

    @After
    fun tearDown() {
        presenter.onDettach()
    }

    @Test
    fun componentShouldShowLoadingWhileUploading() {
        `when`(service.uploadImage(any()))
                .thenReturn(Observable.just(mockImage()))

        val uri = mock(Uri::class.java)
        `when`(uri.path).thenReturn("picture")
        presenter.uploadImage(uri)

        verify(view, atLeast(1)).showLoading()
    }

    @Test
    fun componentShouldHaveTheAbilityToRequestCurrentImage() {
        `when`(service.loadSelectedImage())
                .thenReturn(Observable.just(mockImage()))

        presenter.loadPreviouslySelectedImage()

        verify(view, times(1)).showSelectedImage(any())
    }

    @Test
    fun componentShouldShowPlaceholder() {
        `when`(service.loadSelectedImage())
                .thenReturn(Observable.error(RuntimeException()))

        presenter.loadPreviouslySelectedImage()

        verify(view, times(1)).showPlaceholder()
    }

    private fun mockImage(id: Int = 1) = Image(id = id,
            picture = "http://image/picture")

}