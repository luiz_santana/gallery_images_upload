package net.luizsantana.onlinegallery.gallery.presenters

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import net.luizsantana.onlinegallery.gallery.contracts.GalleryListContract
import net.luizsantana.onlinegallery.gallery.models.Image
import net.luizsantana.onlinegallery.gallery.services.GalleryService
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

/**
 * Created by luiz on 04/09/17.
 */
class GalleryImageListPresenterTests {

    lateinit var service: GalleryService
    lateinit var view: GalleryListContract.View
    lateinit var presenter: GalleryListPresenterImpl

    @Before
    fun setUp() {
        service = mock(GalleryService::class.java)
        view = mock(GalleryListContract.View::class.java)
        presenter = GalleryListPresenterImpl(view, service)
        presenter.onAttach()

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    }

    @After
    fun tearDown() {
        presenter.onDettach()
    }

    @Test
    fun presenterShouldHaveTheAbilityToLoadImages() {
        val listOfImages = listOf(mockImage(1),
                mockImage(2),
                mockImage(3),
                mockImage(4))
        `when`(service.listImages())
                .thenReturn(Observable.just(
                        listOfImages))

        presenter.loadData()

        verify(view, times(1)).showImageData(any())
    }

    @Test
    fun presenterShouldShowLoadingWhileFetchingImages() {
        val listOfImages = listOf(mockImage(1),
                mockImage(2),
                mockImage(3),
                mockImage(4))
        `when`(service.listImages())
                .thenReturn(Observable.just(
                        listOfImages))

        presenter.loadData()

        verify(view, times(1)).showLoading()
    }

    @Test
    fun presenterShouldShowEmptyMessageWhenEmptyList() {
        `when`(service.listImages())
                .thenReturn(Observable.just(emptyList()))

        presenter.loadData()

        verify(view, times(1)).showEmptyListMessage()
    }

    private fun mockImage(id: Int = 1) = Image(id = id,
            picture = "http://image/picture")

}