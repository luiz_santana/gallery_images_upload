package net.luizsantana.onlinegallery.gallery.helpers

import net.luizsantana.onlinegallery.gallery.models.Image

/**
 * Created by luiz on 11/09/17.
 */
fun mockImage(id: Int = 1) = Image(id = id,
        picture = "http://image/picture",
        favorite = false)